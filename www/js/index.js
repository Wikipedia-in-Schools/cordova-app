/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
  // Application Constructor
  initialize : function() {
    this.bindEvents();
  },
  initApplication : function() {
    $(document).on('pageinit', '#webviewer-page', function() {
      var url = this.getAttribute('data-url').replace(/(.*?)link=/g, '');
      app.initWebViewerPage(url);
    }).on('pageinit', '#pdfviewer-page', function() {
      var url = this.getAttribute('data-url').replace(/(.*?)link=/g, '');
      app.initPDFViewerPage(url);
    }).on(
        'pageinit',
        '#list-explorer-page',
        function() {
          var categParent = this.getAttribute('data-url').replace(
              /(.*?)parent=/g, '');
          app.initListExplorerPage(categParent);
        });
    app.initListExplorerPage('');
    // Application.openLinksInApp();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents : function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicitly call 'app.receivedEvent(...);'
  onDeviceReady : function() {
    app.initApplication();
  },
  initWebViewerPage : function(url) {
    //console.log("url " + url);
    $('#web-view-frame').attr("src", url);
  },
  initPDFViewerPage : function(url) {
    //console.log("url " + url);
    // $('#web-view-frame').attr("src", url);
    if (!PDFJS.PDFViewer || !PDFJS.getDocument) {
      console.log('Please build the pdfjs-dist library using\n'
          + '  `gulp dist`');
    }
    // The workerSrc property shall be specified.
    PDFJS.workerSrc = 'js/pdfjs/build/pdf.worker.js';

    // var DEFAULT_URL = '../../web/compressed.tracemonkey-pldi-09.pdf';
    //var DEFAULT_URL = 'http://172.27.81.10:3000/js/chapter_1.pdf';
    var DEFAULT_URL = url;
    var SEARCH_FOR = ''; // try 'Mozilla';

    var container = document.getElementById('viewerContainer');

    // (Optionally) enable hyperlinks within PDF files.
    var pdfLinkService = new PDFJS.PDFLinkService();

    var pdfViewer = new PDFJS.PDFViewer({
      container : container,
      linkService : pdfLinkService,
    });
    pdfLinkService.setViewer(pdfViewer);

    // (Optionally) enable find controller.
    var pdfFindController = new PDFJS.PDFFindController({
      pdfViewer : pdfViewer
    });
    pdfViewer.setFindController(pdfFindController);

    container.addEventListener('pagesinit', function() {
      // We can use pdfViewer now, e.g. let's change default scale.
      pdfViewer.currentScaleValue = 'page-width';

      if (SEARCH_FOR) { // We can try search for things
        pdfFindController.executeCommand('find', {
          query : SEARCH_FOR
        });
      }
    });

    // Loading document.
    PDFJS
        .getDocument(DEFAULT_URL)
        .then(
            function(pdfDocument) {
              // Document loaded, specifying document for the viewer and
              // the (optional) linkService.
              pdfViewer.setDocument(pdfDocument);

              pdfLinkService.setDocument(pdfDocument, null);

              setTimeout(
                  function() {
                    //console.log("setTimeout on load");
                    $('div.textLayer div')
                        .each(
                            function() {
                              var contentfull = $(this).html();
                              // console.log(contentfull);
                              var newcontent = contentfull
                                  .replace(/the/g,
                                      '<a style="background:red; margin-left: -2px;" href="#">the </a>');
                              //console.log(newcontent);
                              $(this).html(newcontent);
                            });

                  }, 4000);
            });
  },
  // Download Categories JSON API
  initListExplorerPage : function(categParent) {
    $.ajaxSetup({
      cache : false
    });
    var api_url = "http://wikipediainschools.org/api/v1/categories/";
    // console.log("test " + api_url);
    $
        .ajax({
          url : api_url,
          dataType : 'JSONP',
          jsonpCallback : 'callback',
          type : 'GET',
          success : function(categoriesObj) {
            // console.log(result);
            /*
             * var decoded = $('<div/>').html(result).text(); categoriesObj =
             * JSON.parse(decoded);
             */

            var hyphened_categParent = categParent.replace(/\//g, '-');
            // Replace spaces with -
            hyphened_categParent = hyphened_categParent.replace(/\s+/g, '-');
            $('#contents-list').attr("id",
                "contents-list-" + hyphened_categParent);
            var contentsListView = $('#contents-list-' + hyphened_categParent);
            if (categParent) {
              var categParentArray = categParent.split("/");
              $.each(categParentArray, function(i, el) {
                categoriesObj = categoriesObj[el]["items"];
              });
            }

            if (categoriesObj) {
              $
                  .each(
                      categoriesObj,
                      function(i, el) {
                        var parentPath = i;
                        if (categParent) {
                          parentPath = categParent + "/" + i;
                        }
                        var image_html = '';
                        if (el.thumbnails) {
                          image_html = '<img height="230px" src="http://wikipediainschools.org'
                              + el.thumbnails[0] + '" />';
                        }
                        var htmlItems = '<li><a href="index.html?parent='
                            + parentPath + '">' + image_html + i + '</a></li>';
                        contentsListView.append(htmlItems);
                      });
            }
            contentsListView.listview('refresh');
            if (categParent) {
              // var api_url =
              // "http://wikipediainschools.org/api/v1/categories/English/Wikipedia/Mathematics/Discrete%20mathematics/Coding%20theory/";
              var api_url = "http://wikipediainschools.org/api/v1/categories/"
                  + categParent + "/";
              $.ajax({
                url : encodeURI(api_url),
                dataType : 'JSONP',
                jsonpCallback : 'callback',
                type : 'GET',
                success : function(pagesObj) {
                  /*
                   * var decoded = $('<div/>').html(result).text();
                   * categoriesObj = JSON.parse(decoded);
                   */
                  $.each(pagesObj, function(i, el) {
                    var image_html = '';
                    if (el.thumbnails) {
                      image_html = '<img height="230px" src="'
                          + el.thumbnails[0] + '" />';
                    }
                    if (el.article_ref.endsWith(".pdf")) {
                      var htmlItems = '<li><a href="pdfviewer.html?link='
                          + el.article_ref + '">' + image_html + el.title
                          + '</a></li>';
                      contentsListView.append(htmlItems);
                    } else {
                      var htmlItems = '<li><a href="webviewer.html?link='
                          + el.article_ref + '">' + image_html + el.title
                          + '</a></li>';
                      contentsListView.append(htmlItems);
                    }
                  });
                  contentsListView.listview('refresh');
                }
              });
            }
          }
        });
  }
};

app.initialize();